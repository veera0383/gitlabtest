package com.yalla.selenium.api.base;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
//import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.InvalidElementStateException;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.yalla.selenium.api.design.Browser;
import com.yalla.selenium.api.design.Element;

import utils.Reports;


public class SeleniumBase extends Reports implements Browser, Element{

	public RemoteWebDriver driver;
	WebDriverWait wait;
	String parentWindow;
	int i=1;
	@Override
	public void click(WebElement ele) {
		try {
			wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(ele));			
			ele.click();
			reportStep("The Element ("+ele+") clicked", "pass");			
		} catch (StaleElementReferenceException e) {
			reportStep("The Element ("+ele+") could not beclicked", "fail");			 
			throw new RuntimeException();
		}finally {
			System.out.println("Snapshot Taken after Click operation");
			takeSnap();
		}
	}

	@Override
	public void append(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			reportStep("The given Data ("+data+") is successfully entered in ("+ele+") text field", "pass");
		}catch (ElementNotInteractableException e) {
			reportStep("The Element ("+ele+") is Not interactable/Editable", "fail");			 
			throw new RuntimeException();
		}catch(IllegalArgumentException e){
			reportStep("The given value ("+data+") is Not vaild one/Empty", "fail");
			
		}finally {
				takeSnap();
		}

	}

	@Override
	public void clear(WebElement ele) {
		try {
			ele.clear();
			reportStep("The Given Element ("+ele+") value get cleared","pass");						
		}catch(InvalidElementStateException e) {			
			reportStep("The Given Element ("+ele+") is not editable","fail");			
		}finally {
			System.out.println("Snapshot Taken after Clear Text Field");
			takeSnap();
		}

	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		try {
			ele.clear();
			ele.sendKeys(data);
			reportStep("The Data ("+data+") entered Successfully","pass");
		} catch(ElementNotInteractableException e) {
			reportStep("The Element ("+ele+") is not Interactable","fail");
			throw new RuntimeException();
		}finally {
			System.out.println("Snapshot Taken after Clear and Enter the value Text Field");
			takeSnap();
		}

	}
	@Override
	public String getElementText(WebElement ele) {
		try{
			String getText=ele.getText();			
			reportStep("The Visible Text ("+getText+") of the element ("+ele+") is fetched Successfully","pass");
			return getText;
		
		}catch(NoSuchElementException e) {
			reportStep("The Element ("+ele+") is not availabel","fail");	
		}catch(StaleElementReferenceException e) {
			reportStep("The Element ("+ele+") is not yet loaded","fail");	
		}
		return null;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		try {
			WebElement coLor = driver.findElementById("color");
			String bgcolor = coLor.getCssValue("background-color");
			reportStep("BackgroundColor for the given element ("+ele+") is fetched successfully ","pass");
			return bgcolor;
		} catch (Exception e) {
			reportStep("Unable to fetch BackgroundColor for the given element ("+ele+")","fail");
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		try {
			String TypedText = ele.getAttribute("value");
			reportStep("The given Element ("+ele+") typed text value ("+TypedText+") is fetched successfully","pass");
			return TypedText;
		} catch (NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return null;
		}
				
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
		Select select = new Select(ele);
		select.selectByVisibleText(value);		
		reportStep("The Selection with visible text ("+value+") is Successfully made","pass");
		System.out.println();
		}catch(NoSuchElementException e) {
			reportStep("The given Element ("+value+") is not available in the list","fail");
			throw new RuntimeException();
		}catch(StaleElementReferenceException e) {
			reportStep("The List ("+ele+") is not yet loaded","fail");
		}


	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index){
		try {
		Select select = new Select(ele);
		select.selectByIndex(index);		
		reportStep("The Selection with index ("+index+") is Successfully made","pass");
		}catch(StaleElementReferenceException e) {
			reportStep("The List ("+ele+") is not yet loaded","fail");
		}catch(NoSuchElementException e) {
			reportStep("The provided index ("+index+") value is not exist in the list","fail");
			throw new RuntimeException();
		}
		
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		try {
			Select select = new Select(ele);
			select.selectByValue(value);
			reportStep("The Selection with Value ("+value+") is Successfully made","pass");
			}catch(NoSuchElementException e) {
				reportStep("The given Element ("+value+") is not available in the list","fail");
				throw new RuntimeException();
			}catch(StaleElementReferenceException e) {
				reportStep("The List ("+ele+") is not yet loaded","fail");
			}

	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		String Actualtext = ele.getText();
		try {
		if(Actualtext.equals(expectedText)) {
			reportStep("The Actual Text ("+Actualtext+")  is equal with Expected Text ("+expectedText+")","pass");
			return true;}
		else {
			reportStep("The Actual Text ("+Actualtext+") is not equal with Expected Text ("+expectedText+")","pass");
			return false;
		}
		}catch(NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		String Actualtext = ele.getText();
		try {
		if(Actualtext.contains(expectedText)) {
			reportStep("The Actual Text ("+Actualtext+") is cotains Expected Text ("+expectedText+")","pass");
			return true;}
		else {
			reportStep("The Actual Text ("+Actualtext+") is not having Expected Text ("+expectedText+")","fail");
			return false;
		}
		}catch(NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		String Attributevalue = ele.getAttribute(attribute);
		try {
		if(Attributevalue.equals(value)) {
			reportStep("The Actual Attribute value ("+Attributevalue+") is equls with Expected value ("+value+")","pass");
			return true;}
		else {
			reportStep("The Actual Attribute value ("+Attributevalue+") is equls with Expected value ("+value+")","fail");	
			return false;
		}
		}catch(NoSuchElementException e) {
			reportStep("The given Attribute "+attribute+" is not available for the element "+ele,"fail");
			return false;
		}		
	}

	@Override
	public boolean verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String Attributevalue = ele.getAttribute(attribute);
		try {
		if(Attributevalue.contains(value)) {
			reportStep("The Actual Attribute value"+Attributevalue+" contains Expected value "+value,"pass");
			return true;}
		else {
			reportStep("The Actual Attribute value"+Attributevalue+" is not having Expected value "+value,"fail");	
			return false;
		}
		}catch(NoSuchElementException e) {
			reportStep("The given Attribute "+attribute+" is not available for the element "+ele,"fail");
			return false;
		}

	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		boolean displayed = ele.isDisplayed();
		try {
			if(displayed==true) {
			reportStep("The given Element "+ele+" is displayed","pass");
			return true;
			}else {
				reportStep("The given Element "+ele+" is not displayed","fail");
				return false;
			} 
		} catch (NoSuchElementException e) {
			reportStep("The given Element "+ele+" is not available","fail");
			return false;
		}
		
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		boolean displayed = ele.isDisplayed();
		try {
			if(displayed==false) {
			reportStep("The given Element ("+ele+") is not displayed","pass");
			return true;
			}else {
				reportStep("The given Element ("+ele+") is displayed","fail");
				return false;
			} 
		} catch (NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		boolean enabled = ele.isEnabled();
		try {
			if(enabled==true) {
			reportStep("The given Element ("+ele+") is enabled","pass");
			return true;
			}else {
				reportStep("The given Element ("+ele+") is not enabled","fail");
				return false;
			} 
		} catch (NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}finally {
			takeSnap();
		}
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		try {
			boolean selected = ele.isSelected();
			if(selected==true) {
			reportStep("The given Element ("+ele+") is displayed","pass");
			return true;
			}else {
				reportStep("The given Element ("+ele+") is not displayed","fail");
				return false;
			} 
		} catch (NoSuchElementException e) {
			reportStep("The given Element ("+ele+") is not available","fail");
			return false;
		}finally {
			takeSnap();
		}
	}

	@Override
	public void startApp(String url) {
		try{
			System.setProperty("webdriver.chrome.driver",
					"./drivers/chromedriver.exe");
			driver = new ChromeDriver();
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}catch (Exception e) {			
			reportStep("The Browser Could not be Launched. Hence Failed","fail");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}
	}

	@Override
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver",
						"./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if(browser.equalsIgnoreCase("firefox")) {
				System.setProperty("webdriver.gecko.driver",
						"./drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			} else if(browser.equalsIgnoreCase("ie")) {
				System.setProperty("webdriver.ie.driver",
						"./drivers/IEDriverServer.exe");
				driver = new InternetExplorerDriver();
			}
			driver.navigate().to(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		} catch (Exception e) {
			reportStep("The Browser Could not be Launched. Hence Failed","fail");
			throw new RuntimeException();
		}finally {
			takeSnap();
		}

	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		try {
			switch(locatorType.toLowerCase()) {
			case "id": return driver.findElementById(value);
			case "name": return driver.findElementByName(value);
			case "class": return driver.findElementByClassName(value);
			case "link": return driver.findElementByLinkText(value);
			case "xpath": return driver.findElementByXPath(value);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator ("+locatorType+") Not Found with value ("+value+")","fail");
			
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
		return null;
	}

	@Override
	public WebElement locateElement(String value) {
		try {
			WebElement ElementbyId = driver.findElementById(value);
			reportStep("The Element is Found with Id value: ("+value+")","pass");
			return ElementbyId;
		} catch (NoSuchElementException e) {
			reportStep("The Element is Not Found with Id value: ("+value+")","fail");
			return null;
		}finally {
			takeSnap();
		}	
	}
	
	
	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch(type.toLowerCase()) {			
			case "id": return (List<WebElement>) driver.findElementsById(value);
			case "name": return (List<WebElement>) driver.findElementsByName(value);
			case "class": return (List<WebElement>) driver.findElementsByClassName(value);			
			case "xpath": return (List<WebElement>)  driver.findElementsByXPath(value);
			}
		} catch (NoSuchElementException e) {
			reportStep("The Element with locator ("+type+") Not Found with value ("+value+")","fail");
			
			throw new RuntimeException();
		}
		finally {
			takeSnap();
		}
		return null;
	}
	

	@Override
	public void switchToAlert() {		
		try {
			driver.switchTo().alert();
			reportStep("Successfully Switch to Alert window ","pass");
		} catch (NoAlertPresentException e) {
			reportStep("The Expected Alert is not Present","fail");			
		}
	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			reportStep("Successfully Accept the Alert window ","pass");
		} catch (NoAlertPresentException e) {
			reportStep("The Expected Alert is not Present","fail");
		}
	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();;
			reportStep("Successfully dismiss the Alert window ","pass");
		} catch (NoAlertPresentException e) {
			reportStep("The Expected Alert is not Present","fail");
		}

	}

	@Override
	public String getAlertText() {
		
		try {
			String AlertText = driver.switchTo().alert().getText();
			reportStep("Alert Text is Successfully fetched the Alert window ","pass");
			return AlertText;
		} catch (NoAlertPresentException e) {
			reportStep("The Expected Alert is not Present to fetch text from it","fail");			
			return null;
		}
		
	}

	@Override
	public void typeAlert(String data) {		
		try {
			driver.switchTo().alert().sendKeys(data);
			reportStep("The given Data ("+data+") is Successfully passed in Alert window ","pass");
			
		}catch (NoAlertPresentException e) {
			reportStep("The Expected Alert is not Present to pass data in it","fail");			
		}
	}

	@Override
	public void switchToWindow(int index) {
		try{
		System.out.println("Inside Window Handler");
		Set<String> windows = driver.getWindowHandles();
		List<String> windowsList=new ArrayList<>();		
		windowsList.addAll(windows);
		System.out.println(windowsList.size());
		driver.switchTo().window(windowsList.get(index));
		reportStep("Successfully Switch to Window with index "+index,"pass");
		}catch(NoSuchWindowException e) {
			reportStep("The window is not displayed with given index ("+index+") value","fail");
		}
	}

	@Override
	public void switchToWindow(String title) {		
		try {
			driver.switchTo().window(title);
			reportStep("Successfully Switch to Window with the Given Title ("+title+")","pass");
		} catch (NoSuchWindowException e) {			
			reportStep("The window is not displayed with the Given Title ("+title+")","fail");
		}
	}
	@Override
	public void switchToFrame(int index) {
		try {			
		driver.switchTo().frame(index);
		reportStep("Successfully Switch to Frame with index ("+index+")","pass");
		}catch(NoSuchFrameException e) {
			reportStep("The Frame is not displayed with given index value ("+index+")","fail");
		}
	}

	@Override
	public void switchToFrame(WebElement ele) {
		try {			
			driver.switchTo().frame(ele);
			reportStep("Successfully Switch to Frame by WebElement ("+ele+")","pass");
			}catch(NoSuchFrameException e) {
				reportStep("No Such Frame is  available with given WebElement value ("+ele+")","fail");
			}catch(StaleElementReferenceException e) {
				reportStep("The Frame is not displayed/loaded yet with given WebElement value ("+ele+")","fail");
			}
	}

	@Override
	public void switchToFrame(String idOrName) {
		try {			
			driver.switchTo().frame(idOrName);
			reportStep("Successfully Switch to Frame by idOrName ("+idOrName+")","pass");
			}catch(NoSuchFrameException e) {
				reportStep("The Frame is not displayed with given idOrName value ("+idOrName+")","fail");
			}

	}

	@Override
	public void defaultContent() {
		try {
			driver.switchTo().defaultContent();
			reportStep("Successfully Switch to Default Frame","pass");
			
		} catch (NoSuchFrameException e) {
			reportStep("Default Frame is not Displayed","fail");
		}

	}

	@Override
	public boolean verifyUrl(String url) {
		try{
			String currentUrl = driver.getCurrentUrl();
			if(currentUrl.equals(url)) {				
				reportStep("The CurrentUrl ("+currentUrl+") is equals with given Url ("+url+")","pass");
				return true;
			}else {
				reportStep("The CurrentUrl ("+currentUrl+") is not equals with given Url ("+url+")","pass");
				return false;
			}
		}catch(Exception e) {
			reportStep("Unable to Fetch Url","fail");
			return false;
		}
		
		
	}

	@Override
	public boolean verifyTitle(String title) {
		try{
			String currentTitle = driver.getTitle();
			if(currentTitle.equals(title)) {				
				reportStep("The CurrentUrl ("+currentTitle+") is equals with given Url ("+title+")","pass");
				return true;
			}else {
				reportStep("The CurrentUrl ("+currentTitle+") is not equals with given Url ("+title+")","pass");
				return false;
			}
		}catch(Exception e) {
			reportStep("Unable to Fetch current Title to verify with Given Title","fail");
			return false;
		}
		
	}

	@Override
	public void takeSnap(){
		try{
			File src=driver.getScreenshotAs(OutputType.FILE);
			File dest=new File("./snaphsots/snap"+i+".png");			
			FileUtils.copyFile(src, dest);
			reportStep("Screenshot captured Successfully","pass");
		}catch(IOException e){			
			reportStep("Screenshot could not be captured"+e.toString(),"fail");
		}
		i++;
	}

	@Override
	public void close() {		
		try {
			driver.close();
			reportStep("The Browser has been closed Successfully","pass");			
		} catch (Exception e) {
			reportStep("Unable to Close the Browser","fail");
		}

	}

	@Override
	public void quit() {
		try {
			driver.quit();
			reportStep("The Browser has been quited Successfully","pass");			
		} catch (Exception e) {
			reportStep("Unable to quit from the Browser","fail");
		}

	}

	@Override
	public String getWindowTitle() {
		String title = driver.getTitle();
		return title;
		
	}

	@Override
	public void switchToParentWindow(String WindowName) {
		
		try {
			driver.switchTo().window(WindowName);
			reportStep("Successfully Switch to ParentWindow","pass");
		} catch (Exception e) {
			reportStep("Unable to Switch to Parent Window","fail");
			
		}
		
	}

	@Override
	public String getParentWindow() {
		try {
			String ParentWindow = driver.getWindowHandle();		
			reportStep("ParentWindow Name("+driver.getCurrentUrl()+") is Fetched Successfully","pass");
			return ParentWindow;
		} catch (Exception e) {
			reportStep("ParentWindow Name("+driver.getCurrentUrl()+") is not available","fail");
			return null;
		}
	}

	

}
