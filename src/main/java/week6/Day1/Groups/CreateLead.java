package week6.Day1.Groups;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class CreateLead extends Annotations{
	String FirstLead="10897",SecondLead="10898";
	
	
	@BeforeTest(groups={"any"})
	public void setData() {
		testcaseName= "TC001_CreateLead";
		testcaseDec = "Create a new Lead in leaftaps";
		author      = "Veera";
		category    = "Smoke";
	}
	
	@Test(groups={"smoke"})
	
	public void login() throws InterruptedException {
		WebElement eleCreateLead=locateElement("link","Create Lead");
		click(eleCreateLead);
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		clearAndType(eleCompanyName, "TLCompany");		
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		clearAndType(eleFirstName, "TLFirst");
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		clearAndType(eleLastName, "TLLast");		
		WebElement eleSrcDD = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(eleSrcDD,1);
		WebElement eleSubmit=locateElement("name","submitButton");
		System.out.println(eleSubmit);
		click(eleSubmit);
		//Merge Lead Start Here
		
		/*WebElement eleMergeLead=locateElement("link","Merge Leads");
		click(eleMergeLead);
		//From Lead Search
		String windowTitle=getWindowTitle();
		System.out.println("WindowTitle :"+windowTitle);
		WebElement eleFLead=locateElement("xpath","//input[@id='ComboBox_partyIdFrom']/following::a[1]");
		click(eleFLead);
		switchToWindow(1);
		WebElement eleSearchFLead=locateElement("name", "id");
		clearAndType(eleSearchFLead,FirstLead);
		WebElement FindLeadsButton=locateElement("xpath","//button[text()='Find Leads']");
		click(FindLeadsButton);
		Thread.sleep(3000);
		//WebElement FLeadLink=locateElement("link",FirstLead);
		
		//click(FLeadLink);
		
		
		//*********
		WebElement eleselopt1=locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]");
		click(eleselopt1);
		//switchToWindow(0);
		
		
		//***************
		Thread.sleep(5000);
		switchToWindow(0);		
		//To Lead Search		
		WebElement eleSLead=locateElement("xpath","//input[@id='ComboBox_partyIdTo']/following::a[1]");
		click(eleSLead);
		switchToWindow(1);
		WebElement eleSearchSLead=locateElement("name", "id");
		clearAndType(eleSearchSLead,SecondLead);
		WebElement FindLeadSButton=locateElement("xpath","//button[text()='Find Leads']");
		click(FindLeadSButton);
		Thread.sleep(5000);
		WebElement SLeadLink=locateElement("xpath","//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[1]");
		click(SLeadLink);
		switchToWindow(0);
		WebElement MergeButton=locateElement("link","Merge");
		click(MergeButton);
		acceptAlert();
		
		*/
		//driver.close();
		

		
	}
}







